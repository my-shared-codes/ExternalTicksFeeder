#include <algorithm>
#include <QByteArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QDebug>
#include <QPushButton>
#include <QLineEdit>
#include <QHeaderView>
#include <QThread>
#include <QTimeZone>
#include <QThreadPool>
#include <QFuture>
#include <QFutureSynchronizer>
#include <QtConcurrent>

#include "QColoredProgressBar.h"

#include "QTreeWidget.h"
#include "ExternalTicksFeeder.h"
#include "ITickProvider.h"
#include "QTickProviderFactory.h"

ExternalTicksFeeder::ExternalTicksFeeder(QWidget* parent):
QMainWindow(parent),
mPort(5533),
mZMQContext(nullptr),
mWasExpandAllOrCollapseAll(false),
mThreadPool(nullptr)
{
	mThreadPool = new QThreadPool(this);
	mThreadPool->setMaxThreadCount(32);

	ui.setupUi(this);

	connect(ui.tabWidget, &QTabWidget::currentChanged, this, &ExternalTicksFeeder::on_TabWidget_currentChanged);

	connect(ui.expanderPushButton, &QPushButton::clicked, this, &ExternalTicksFeeder::on_ExpanderPushButton_clicked);
	connect(ui.collapserPushButton, &QPushButton::clicked, this, &ExternalTicksFeeder::on_CollapserPushButton_clicked);
	connect(ui.filterTypePushButton, &QPushButton::clicked, this, &ExternalTicksFeeder::on_FilterTypePushButton_clicked);
	connect(ui.filterLineEdit, &QLineEdit::textChanged, this, &ExternalTicksFeeder::on_FilterLineEdit_textChanged);
	connect(ui.horizontalSlider, &QSlider::valueChanged, this, &ExternalTicksFeeder::on_HorizontalSlider_valueChanged);

	mZMQContext = nzmqt::createDefaultContext(this);
	createNextSocket();
	mZMQContext->start();

	delete ui.progressBar;

		QColoredProgressBar* cpb = new QColoredProgressBar(ui.tabWidget->widget(0));
		ui.progressBar = cpb;
		cpb->move(ui.horizontalSlider->pos().x(), ui.horizontalSlider->pos().y() + 50);
		cpb->resize(ui.horizontalSlider->size());
		cpb->setVisible(true);
		cpb->setGradientStyle1();
}

void ExternalTicksFeeder::resizeEvent(QResizeEvent* e)
{
	ui.tabWidget->setVisible(false);
	ui.tabWidget->resize(e->size().width() - 20, e->size().height() - 50);

	for (ITickProvider* i : mProviders)
	{
		i->mTreeWidget->resize(ui.tabWidget->size().width() - 20, ui.tabWidget->size().height() - 20);
	}

	ui.tabWidget->setVisible(true);
	ui.filterLineEdit->resize(e->size().width() - 110, 20);
}

void ExternalTicksFeeder::fixTreeWidgetSize(QTreeWidget* treeWidget)
{
	treeWidget->header()->resizeSections(QHeaderView::ResizeToContents);
	for (int i = 0; i < treeWidget->columnCount(); ++i) treeWidget->resizeColumnToContents(i);
}

void ExternalTicksFeeder::createNextSocket()
{
	QString port = QString::number(mPort);
	nzmqt::ZMQSocket* socket = mZMQContext->createSocket(nzmqt::ZMQSocket::TYP_PAIR, mZMQContext);

	socket->setLinger(5);
	socket->setOption(nzmqt::ZMQSocket::OPT_IMMEDIATE, 1);
	socket->setIdentity(port);
	socket->bindTo("tcp://127.0.0.1:" + QString::number(mPort++));

	connect(socket, &nzmqt::ZMQSocket::messageReceived, this, &ExternalTicksFeeder::on_ZMQSocket_messageReceived);
}

void ExternalTicksFeeder::on_ZMQSocket_messageReceived(const QList<QByteArray>& msgList)
{
	nzmqt::ZMQSocket* socket = (nzmqt::ZMQSocket*)QObject::sender();
	QJsonDocument json = QJsonDocument::fromJson(msgList[0]);

	if (json["type"] == "ping")
	{
		QJsonObject reply;
		reply["type"] = "pong";
		socket->sendMessage(QJsonDocument(reply).toJson());
	}
	else if (json["type"] == "provider")
	{
		QString name = json["name"].toString().trimmed();

		if (mProviders.find(name) != mProviders.end()) return;

		createNextSocket();

		ITickProvider* tickProvider = QTickProviderFactory::createTickProvider(name, mZMQContext);

		socket->setParent(tickProvider);
		socket->setIdentity(name);

		tickProvider->mExternalTicksFeeder = this;
		tickProvider->mDescripton = name;
		tickProvider->mZMQSocket = socket;
		tickProvider->mCustomSymbolPrefix = json["customSymbolPrefix"].isNull() ? "" : json["customSymbolPrefix"].toString().trimmed();
		tickProvider->mCustomSymbolPostfix = json["customSymbolPostfix"].isNull() ? "" : json["customSymbolPostfix"].toString().trimmed();
		tickProvider->isMT5Provider = false;

		mProviders[name] = tickProvider;

		createTreeView(tickProvider);
	}
	else if (json["type"] == "symbols")
	{
		QString name = "MT5 Terminal";

		if (mProviders.find(name) != mProviders.end()) return;

		ITickProvider* tickProvider = QTickProviderFactory::createTickProvider(name, socket);

		socket->setParent(tickProvider);
		socket->setIdentity(name);

		tickProvider->mExternalTicksFeeder = this;
		tickProvider->mDescripton = name + " (" + json["description"].toString().trimmed() + ")";
		tickProvider->mZMQSocket = socket;
		tickProvider->mCustomSymbolPrefix = "";
		tickProvider->mCustomSymbolPostfix = "";
		tickProvider->isMT5Provider = true;

		int symbolsCount = json["symbolsCount"].toInt();
		tickProvider->mSymbols.reserve(symbolsCount);

		for (uint32_t i = 0; i < symbolsCount; ++i)
		{
			QSymbol symbol;
			symbol.mName = json["names"][i].toString().trimmed();
			symbol.mPath = json["paths"][i].toString().trimmed();
			symbol.mDescription = QByteArray::fromBase64(json["descriptions"][i].toString().toLocal8Bit()).trimmed();
			symbol.mDigits = json["digits"][i].toInt();

			int y = 0; int M = 0; int d = 0; int h = 0; int m = 0; int s = 0;
			if (sscanf(json["toDateTimes"][i].toString().trimmed().toLocal8Bit().constData(), "%04d.%02d.%02d %02d:%02d:%02d", &y, &M, &d, &h, &m, &s) != 6) symbol.mToDateTime = QDateTime::currentDateTime();
			else symbol.mToDateTime = QDateTime(QDate(y, M, d), QTime(h, m, s));

			tickProvider->mSymbols.push_back(symbol);
		}

		tickProvider->mSymbols.shrink_to_fit();
		qStableSort(tickProvider->mSymbols.begin(), tickProvider->mSymbols.end(), &QSymbol::lessThen);

		mProviders[name] = tickProvider;

		createTreeView(tickProvider);
	}
}

void ExternalTicksFeeder::createTreeView(ITickProvider* tickProvider)
{
	tickProvider->mTreeWidget = new QTreeWidget(ui.tabWidget);
	connect(tickProvider->mTreeWidget, &QTreeWidget::itemChanged, this, &ExternalTicksFeeder::on_TreeWidget_itemChanged);
	connect(tickProvider->mTreeWidget, &QTreeWidget::itemExpanded, this, &ExternalTicksFeeder::on_TreeWidget_itemExpanded);
	connect(tickProvider->mTreeWidget, &QTreeWidget::itemCollapsed, this, &ExternalTicksFeeder::on_TreeWidget_itemCollapsed);

	tickProvider->mTreeWidget->move(10, 40);
	tickProvider->mTreeWidget->resize(ui.tabWidget->size().width() - 20, ui.tabWidget->size().height() - 50);

	tickProvider->mTreeWidget->setUniformRowHeights(true);
	tickProvider->mTreeWidget->setAlternatingRowColors(true);

	if (tickProvider->isMT5Provider) ui.tabWidget->insertTab(1, tickProvider->mTreeWidget, tickProvider->mName);
	else
	{
		int newTabIndex = 1;
		if (mProviders.keys().contains("MT5 Terminal")) newTabIndex++;

		for (ITickProvider* i : mProviders)
		{
			if (i->mName.compare(tickProvider->mName, Qt::CaseInsensitive) < 0) newTabIndex++;
			else break;
		}
		ui.tabWidget->insertTab(newTabIndex, tickProvider->mTreeWidget, tickProvider->mName);
	}

	if (tickProvider->isMT5Provider)
	{
		tickProvider->mTreeWidget->setHeaderLabels({ tickProvider->mName + " symbol", "Description", "Digits", "Last tick's timestamp" });
	}
	else
	{
		tickProvider->mTreeWidget->setHeaderLabels({ tickProvider->mName + " symbol", "Description", "MT5 cloned symbol (guessed)", "MT5 custom symbol", "Digits", "Download from", "Download to" });
	}


	QTreeWidgetItem* item = new QTreeWidgetItem({ tickProvider->mDescripton });
	if (!tickProvider->isMT5Provider) item->setCheckState(0, Qt::Unchecked);
	tickProvider->mTreeWidget->addTopLevelItem(item);

	fixTreeWidgetSize(tickProvider->mTreeWidget);

	tickProvider->downloadSymbolList();
}

void ExternalTicksFeeder::fillTreeWidgets()
{
	if (mProviders["MT5 Terminal"]->mSymbols.isEmpty()) return;

	if (mProviders["MT5 Terminal"]->mTreeWidget->topLevelItem(0)->childCount() == 0) fillTreeWidget(mProviders["MT5 Terminal"]);

	for (ITickProvider* i : mProviders)
	{
		if (i->mName == "MT5 Terminal" || i->mSymbols.isEmpty()) continue;
		if (i->mTreeWidget->topLevelItem(0)->childCount() == 0)
		{
			guessMT5Symbols(i);
			fillTreeWidget(i);
		}
	}
}

void ExternalTicksFeeder::fillTreeWidget(ITickProvider* tickProvider)
{
	QTreeSetNode<QString> rootNode;

	for (QSymbol& i : tickProvider->mSymbols)
	{
		QStringList folders = i.mPath.split("\\", QString::SkipEmptyParts);
		QTreeSetNode<QString>* parent = &rootNode;

		for (QString& j : folders)
		{
			j = j.trimmed();
			parent = parent->addChild(j);
		}
	}

	rootNode.shrink_to_fit();
	rootNode.print();

	fillTreeWidgetFromTreeSet(&rootNode, tickProvider, tickProvider->mTreeWidget->topLevelItem(0));
	fixTreeWidgetSize(tickProvider->mTreeWidget);
}

QString ExternalTicksFeeder::formatDateTime(const QDateTime& dateTime)
{
	return dateTime.toString("yyyy. MM. dd. hh:mm:ss ") + dateTime.timeZone().id();
}

void ExternalTicksFeeder::fillTreeWidgetFromTreeSet(QTreeSetNode<QString>* node, ITickProvider* tickProvider, QTreeWidgetItem* parent)
{
	if (!node || !tickProvider || !parent) return;

	static int symbolIndex = 0;

	if (node->mData.isEmpty())
	{
		symbolIndex = 0;
		for (QTreeSetNode<QString>* i : node->mChildren) fillTreeWidgetFromTreeSet(i, tickProvider, parent);
		return;
	}

	QTreeWidgetItem* item = nullptr;

	if (!node->mChildren.isEmpty())
	{
		item = new QTreeWidgetItem(parent, { node->mData });
	}
	else
	{
		QStringList row;

		if (tickProvider->isMT5Provider)
		{
			row = QStringList
			({
				node->mData,
				tickProvider->mSymbols[symbolIndex].mDescription,
				QString::number(tickProvider->mSymbols[symbolIndex].mDigits),
				formatDateTime(tickProvider->mSymbols[symbolIndex].mToDateTime)
			});
		}
		else
		{
			row = QStringList
			({
				node->mData,
				tickProvider->mSymbols[symbolIndex].mDescription,
				tickProvider->mSymbols[symbolIndex].mMT5ClonedSymbolName,
				"Custom\\ExternalTicksFeeder\\" + tickProvider->mName + "\\" + tickProvider->mCustomSymbolPrefix + tickProvider->mSymbols[symbolIndex].mPath + tickProvider->mCustomSymbolPostfix,
				QString::number(tickProvider->mSymbols[symbolIndex].mDigits),
				formatDateTime(tickProvider->mSymbols[symbolIndex].mFromDateTime),
				formatDateTime(tickProvider->mSymbols[symbolIndex].mToDateTime)
			});
		}

		item = new QTreeWidgetItem(parent, row);
		symbolIndex++;
	}

	if (!tickProvider->isMT5Provider) item->setCheckState(0, Qt::Unchecked);

	parent->addChild(item);

	for (QTreeSetNode<QString>* i : node->mChildren) fillTreeWidgetFromTreeSet(i, tickProvider, item);
}

void ExternalTicksFeeder::on_TreeWidget_itemChanged(QTreeWidgetItem* item, int column)
{
	handleTreeWidgetCheckBoxes(item, column);
}

void ExternalTicksFeeder::on_TreeWidget_itemExpanded(QTreeWidgetItem * item)
{
	if (mWasExpandAllOrCollapseAll) return;
	fixTreeWidgetSize(item->treeWidget());
}

void ExternalTicksFeeder::on_TreeWidget_itemCollapsed(QTreeWidgetItem * item)
{
	if (mWasExpandAllOrCollapseAll) return;
	fixTreeWidgetSize(item->treeWidget());
}

void ExternalTicksFeeder::handleTreeWidgetCheckBoxes(QTreeWidgetItem* item, int column)
{
	if (item == nullptr) return;

	if (item->checkState(column) == Qt::Checked)
	{
		for (int i = 0; i < item->childCount(); ++i) item->child(i)->setCheckState(column, Qt::Checked);

		if (item->parent() == nullptr) return;

		for (int i = 0; i < item->parent()->childCount(); ++i)
		{
			if (item->parent()->child(i)->checkState(column) == Qt::Unchecked)
			{
				QTreeWidgetItem* j = item->parent();
				while (j != nullptr)
				{
					j->setCheckState(column, Qt::PartiallyChecked);
					j = j->parent();
				}
				return;
			}
		}
		item->parent()->setCheckState(column, Qt::Checked);
	}
	else if (item->checkState(column) == Qt::Unchecked)
	{
		for (int i = 0; i < item->childCount(); ++i)  item->child(i)->setCheckState(column, Qt::Unchecked);

		if (item->parent() == nullptr) return;

		for (int i = 0; i < item->parent()->childCount(); ++i)
		{
			if (item->parent()->child(i)->checkState(column) == Qt::Checked)
			{
				QTreeWidgetItem* j = item->parent();
				while (j != nullptr)
				{
					j->setCheckState(column, Qt::PartiallyChecked);
					j = j->parent();
				}
				return;
			}
		}
		item->parent()->setCheckState(column, Qt::Unchecked);
	}
}

void ExternalTicksFeeder::on_ExpanderPushButton_clicked()
{
	mWasExpandAllOrCollapseAll = true;
	QTreeWidget* treeWidget = mProviders[ui.tabWidget->tabText(ui.tabWidget->currentIndex())]->mTreeWidget;
	treeWidget->expandAll();
	fixTreeWidgetSize(treeWidget);
	mWasExpandAllOrCollapseAll = false;
}

void ExternalTicksFeeder::on_CollapserPushButton_clicked()
{
	mWasExpandAllOrCollapseAll = true;
	QTreeWidget* treeWidget = mProviders[ui.tabWidget->tabText(ui.tabWidget->currentIndex())]->mTreeWidget;
	treeWidget->collapseAll();
	fixTreeWidgetSize(treeWidget);
	mWasExpandAllOrCollapseAll = false;
}

void ExternalTicksFeeder::on_TabWidget_currentChanged(int index)
{
	if (ui.tabWidget->tabText(index) == "Settings")
	{
		ui.expanderPushButton->setEnabled(false);
		ui.collapserPushButton->setEnabled(false);
		ui.filterTypePushButton->setEnabled(false);
		ui.filterLineEdit->setEnabled(false);
	}
	else
	{
		ui.expanderPushButton->setEnabled(true);
		ui.collapserPushButton->setEnabled(true);
		ui.filterTypePushButton->setEnabled(true);
		ui.filterLineEdit->setEnabled(true);
		on_FilterLineEdit_textChanged(ui.filterLineEdit->text());
	}
}

void ExternalTicksFeeder::on_FilterTypePushButton_clicked()
{
	if (ui.filterTypePushButton->text() == "T") ui.filterTypePushButton->setText("R");
	else ui.filterTypePushButton->setText("T");
	on_FilterLineEdit_textChanged(ui.filterLineEdit->text());
}

void ExternalTicksFeeder::on_FilterLineEdit_textChanged(const QString& text)
{
	QTreeWidget* treeWidget = mProviders[ui.tabWidget->tabText(ui.tabWidget->currentIndex())]->mTreeWidget;
	if (treeWidget->topLevelItem(0)->childCount() == 0) return;
	filterTreeWidget(treeWidget->topLevelItem(0), text, ui.filterTypePushButton->text());
	fixTreeWidgetSize(treeWidget);
}

void ExternalTicksFeeder::filterTreeWidget(QTreeWidgetItem* item, const QString& text, const QString& type)
{
	if (!item) return;

	if (item->childCount() == 0)
	{
		if (type == "T")
		{
			bool found = false;
			for (int i = 0; item->columnCount() >= 2 && i < 2; ++i)
			{
				if (item->text(i).contains(text, Qt::CaseInsensitive))
				{
					found = true;
					break;
				}
			}
			if (found) item->setHidden(false);
			else item->setHidden(true);
		}
		else
		{
			for (int i = 0; i < item->columnCount(); ++i)
			{
				bool found = false;
				for (int i = 0; item->columnCount() >= 2 && i < 2; ++i)
				{
					if (item->text(i).contains(QRegExp(text)))
					{
						found = true;
						break;
					}
				}
				if (found) item->setHidden(false);
				else item->setHidden(true);
			}
		}
	}

	for (int i = 0; i < item->childCount(); ++i) filterTreeWidget(item->child(i), text, type);
}


/*
*This implementation allows the costs to be weighted, if one of is negative the opeartion is not used:
*
* -w(as in "sWap")
* -s(as in "Substitution")
* -a(for insertion, AKA "Add")
* -d(as in "Deletion")
*/

int ExternalTicksFeeder::calculateLevenshteinDistance(const QString& source, const QString& target, int w, int s, int a, int d)
{
	if (source.length() > target.length()) return calculateLevenshteinDistance(target, source, w, s, a, d);

	std::string s1 = source.toStdString();
	std::string s2 = target.toStdString();
	const char* string1 = &s1[0];
	const char* string2 = &s2[0];

	int len1 = strlen(string1), len2 = strlen(string2);
	int *row0, *row1, *row2;
	int i, j;
	row0 = new int[len2 + 1];
	row1 = new int[len2 + 1];
	row2 = new int[len2 + 1];
	for (j = 0; j <= len2; j++)
		row1[j] = j * a;
	for (i = 0; i < len1; i++) {
		int *dummy;
		row2[0] = (i + 1) * d;
		for (j = 0; j < len2; j++) {
			/* substitution */
			if(s >= 0) row2[j + 1] = row1[j] + s * (string1[i] != string2[j]);
			/* swap */
			if (w >= 0 && i > 0 && j > 0 && string1[i - 1] == string2[j] &&
				string1[i] == string2[j - 1] &&
				row2[j + 1] > row0[j - 1] + w)
				row2[j + 1] = row0[j - 1] + w;
			/* deletion */
			if (d >= 0 && row2[j + 1] > row1[j + 1] + d)
				row2[j + 1] = row1[j + 1] + d;
			/* insertion */
			if (a >= 0 && row2[j + 1] > row2[j] + a)
				row2[j + 1] = row2[j] + a;
		}
		dummy = row0;
		row0 = row1;
		row1 = row2;
		row2 = dummy;
	}
	i = row1[len2];
	delete[] row0;
	delete[] row1;
	delete[] row2;
	return i;
}

double ExternalTicksFeeder::calculateNormalizedLevenshteinDistance(const QString& s1, const QString& s2, int w, int s, int a, int d)
{
	return 1.0 - ((double)calculateLevenshteinDistance(s1, s2, w, s, a, d)) / ((double)std::max(s1.length(), s2.length()));
}

QString ExternalTicksFeeder::normalizeTextForCalculateSimilarity(const QString& text)
{
	QString result;
	result.reserve(text.size());
	for (int i = 0; i < text.length(); ++i) if (isalnum(text[i].toLatin1()) || isspace(text[i].toLatin1())) result.push_back(text[i]);
	return result;
	//static QRegularExpression regexp = QRegularExpression("[^A-Z0-9\\s]");
	//return text.toUpper().remove(regexp);
}

double ExternalTicksFeeder::calculateSimilarity(const QString& text1, const QString& text2, int w, int s, int a, int d)
{
	//return calculateNormalizedLevenshteinDistance(normalizeTextForCalculateSimilarity(text1), normalizeTextForCalculateSimilarity(text2), w, s, a, d);
	return normalizeTextForCalculateSimilarity(text1) == normalizeTextForCalculateSimilarity(text2);
}

void ExternalTicksFeeder::guessMT5Symbols(ITickProvider* tickProvider)
{
	ITickProvider* mt5TickProvider = mProviders["MT5 Terminal"];

	QFutureSynchronizer<void> futureSynchronizer;

	int n = tickProvider->mSymbols.size() / 4;
	for (int k = 0; k < 4; ++k)
	{
		int from = k * n;
		int to = (k + 1) * n;
		if (to > tickProvider->mSymbols.size()) to = tickProvider->mSymbols.size();
		futureSynchronizer.addFuture(QtConcurrent::run(mThreadPool, [this, tickProvider, mt5TickProvider, from, to]()
		{
			for (int i = from; i < to; ++i)
			{
				for (int j = 0; j < mt5TickProvider->mSymbols.size(); ++j)
				{
					QString mt5SymbolName = mt5TickProvider->mSymbols[j].mName;
					if (tickProvider->mName == "Dukascopy")
					{
						if (mt5SymbolName.endsWith(".NAS")) mt5SymbolName.replace(mt5SymbolName.length() - 4, 7, ".US/USD");
						else if (mt5SymbolName.endsWith(".NYSE")) mt5SymbolName.replace(mt5SymbolName.length() - 5, 7, ".US/USD");
					}
					double sim1 = calculateSimilarity(mt5SymbolName, tickProvider->mSymbols[i].mName, -1, 1, 1, 1);
					double sim2 = calculateSimilarity(mt5TickProvider->mSymbols[j].mDescription, tickProvider->mSymbols[i].mDescription, -1, 1, 1, 1);
					double sim = std::max(sim1, sim2);
					if (sim >= 0.72 && mt5TickProvider->mSymbols[j].mMT5ClonedSymbolSimilarity < sim)
					{
						mt5TickProvider->mSymbols[j].mMT5ClonedSymbolSimilarity = sim;
						mt5TickProvider->mSymbols[j].mMT5ClonedSymbolIndex = i;
					}
				}
			}
		}));
	}
	futureSynchronizer.waitForFinished();
	futureSynchronizer.clearFutures();

	for (int i = 0; i < mt5TickProvider->mSymbols.size(); ++i)
	{
		if(mt5TickProvider->mSymbols[i].mMT5ClonedSymbolIndex < 0) continue;
		if (tickProvider->mSymbols[mt5TickProvider->mSymbols[i].mMT5ClonedSymbolIndex].mMT5ClonedSymbolSimilarity < mt5TickProvider->mSymbols[i].mMT5ClonedSymbolSimilarity)
		{
			tickProvider->mSymbols[mt5TickProvider->mSymbols[i].mMT5ClonedSymbolIndex].mMT5ClonedSymbolName = mt5TickProvider->mSymbols[i].mName + " | " + QString::number(mt5TickProvider->mSymbols[i].mMT5ClonedSymbolSimilarity);
			tickProvider->mSymbols[mt5TickProvider->mSymbols[i].mMT5ClonedSymbolIndex].mMT5ClonedSymbolSimilarity = mt5TickProvider->mSymbols[i].mMT5ClonedSymbolSimilarity;
		}
	}
}

void ExternalTicksFeeder::on_HorizontalSlider_valueChanged(int value)
{
	((QColoredProgressBar*)ui.progressBar)->setValue(value);
}
