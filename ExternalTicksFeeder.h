#ifndef EXTERNALTICKSFEEDER_INCLUDED
#define EXTERNALTICKSFEEDER_INCLUDED

#include <QtWidgets/QMainWindow>
#include <QMap>
#include <nzmqt.hpp>

#include "ui_ExternalTicksFeeder.h"
#include "QTreeSet.h"

struct ITickProvider;
class QTreeWidgetItem;
class QTreeWidget;
class QThreadPool;

class ExternalTicksFeeder : public QMainWindow
{
    Q_OBJECT

public:
	ExternalTicksFeeder(QWidget* parent = nullptr);
	void resizeEvent(QResizeEvent* e);
	void fillTreeWidgets();
	void fillTreeWidget(ITickProvider* tickProvider);

private:
	int mPort;
	Ui::ExternalTicksFeederClass ui;
	nzmqt::ZMQContext* mZMQContext;
	QMap<QString, ITickProvider*> mProviders;
	QString mMT5ProviderName;
	bool mWasExpandAllOrCollapseAll;
	QThreadPool* mThreadPool;

	void fixTreeWidgetSize(QTreeWidget* treeWidget);
	void createNextSocket();
	void createTreeView(ITickProvider* tickProvider);
	void fillTreeWidgetFromTreeSet(QTreeSetNode<QString>* node, ITickProvider* tickProvider, QTreeWidgetItem* parent);
	QString formatDateTime(const QDateTime& dateTime);
	void handleTreeWidgetCheckBoxes(QTreeWidgetItem* item, int column);
	void filterTreeWidget(QTreeWidgetItem* item, const QString& text, const QString& type);
	int calculateLevenshteinDistance(const QString& source, const QString& target, int w = 1, int s = 1, int a = 1, int d = 1);
	double calculateNormalizedLevenshteinDistance(const QString& s1, const QString& s2, int w = 1, int s = 1, int a = 1, int d = 1);
	QString normalizeTextForCalculateSimilarity(const QString& text);
	double calculateSimilarity(const QString& text1, const QString& text2, int w = 1, int s = 1, int a = 1, int d = 1);
	void guessMT5Symbols(ITickProvider* tickProvider);

public slots:

	void on_ZMQSocket_messageReceived(const QList<QByteArray>& msgList);
	void on_TreeWidget_itemChanged(QTreeWidgetItem* item, int column);
	void on_TreeWidget_itemExpanded(QTreeWidgetItem* item);
	void on_TreeWidget_itemCollapsed(QTreeWidgetItem* item);
	void on_TabWidget_currentChanged(int index);

	void on_ExpanderPushButton_clicked();
	void on_CollapserPushButton_clicked();
	void on_FilterTypePushButton_clicked();
	void on_FilterLineEdit_textChanged(const QString& text);

	void on_HorizontalSlider_valueChanged(int value);
};

#endif
