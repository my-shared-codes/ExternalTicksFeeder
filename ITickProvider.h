#ifndef ITICKPROVIDER_INCLUDED
#define ITICKPROVIDER_INCLUDED

#include <nzmqt_impl.hpp>

#include <QObject>
#include <QString>
#include <QSet>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QTreeWidget>

#include "QSymbol.h"

class ExternalTicksFeeder;
class QTreeWidgetItem;

struct ITickProvider : public QObject
{
	Q_OBJECT

public:
	enum
	{
		SYMBOL_LIST_DOWNLOAD,
		TICKS_DOWNLOAD,
	};

	QString mName;
	QString mDescripton;
	ExternalTicksFeeder* mExternalTicksFeeder;
	nzmqt::ZMQSocket* mZMQSocket;
	QString mCustomSymbolPrefix;
	QString mCustomSymbolPostfix;
	QVector<QSymbol> mSymbols;
	bool isMT5Provider;
	QTreeWidget* mTreeWidget;
	QNetworkAccessManager* mNetworkaccessmanager;

	ITickProvider() : mExternalTicksFeeder(nullptr), mZMQSocket(nullptr), mNetworkaccessmanager(nullptr), mTreeWidget(nullptr) {}
	virtual ~ITickProvider() {}

	virtual void downloadSymbolList() = 0;

public slots:
	virtual void on_Networkaccessmanager_downloadProgress(qint64 bytesReceived, qint64 bytesTotal) = 0;
	virtual void on_Networkaccessmanager_error(QNetworkReply::NetworkError code) = 0;
	virtual void on_Networkaccessmanager_finished(QNetworkReply* reply) = 0;
	virtual void on_Networkaccessmanager_sslErrors(QNetworkReply* reply, const QList<QSslError>& errors) = 0;
};

#endif
