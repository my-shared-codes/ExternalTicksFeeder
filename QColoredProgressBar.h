#ifndef QCOLOREDPROGRESSBAR_INCLUDED
#define QCOLOREDPROGRESSBAR_INCLUDED

#include <QProgressBar>
#include <QColor>
#include <map>

class QColoredProgressBar : public QProgressBar
{
	Q_OBJECT

public:
	bool colorAtValue(int value, QColor& resultColor)
	{
		if (value < minimum()) value = minimum();
		else if (value > maximum()) value = maximum();
		QProgressBar::setValue(value);

		if (mKeyColors.empty()) return false;
		if (mKeyColors.size() == 1)
		{
			resultColor = mKeyColors.begin()->second;
			return true;
		}
		std::map<int, QColor>::iterator i = mKeyColors.find(value);
		if (i != mKeyColors.end())
		{
			resultColor = i->second;
			return true;
		}

		i = mKeyColors.begin();
		std::map<int, QColor>::iterator j = i; ++j;

		int min = mKeyColors.begin()->first;
		int max = mKeyColors.rbegin()->first;

		if (value < min)
		{
			resultColor = mKeyColors.begin()->second;
			return true;
		}
		else if (value < min)
		{
			resultColor = mKeyColors.rbegin()->second;
			return true;
		}

		for (; j != mKeyColors.end(); ++i)
		{
			j = i;
			++j;
			if (i->first < value && value < j->first) break;
		}

		double v = (double)value;
		double vf = (double)i->first;
		double vt = (double)j->first;

		double rf = (double)i->second.red();
		double gf = (double)i->second.green();
		double bf = (double)i->second.blue();

		double rt = (double)j->second.red();
		double gt = (double)j->second.green();
		double bt = (double)j->second.blue();

		double dr = (rt - rf) / (vt - vf);
		double dg = (gt - gf) / (vt - vf);


		double r = (rt - rf) / (vt - vf) * (v - vf) + rf;
		double g = (gt - gf) / (vt - vf) * (v - vf) + gf;
		double b = (bt - bf) / (vt - vf) * (v - vf) + bf;

		resultColor = QColor::fromRgb((int)r, (int)g, (int)b);
		return true;
	}


	std::map<int, QColor> mKeyColors;
	QColor mGradientRatio;

	explicit QColoredProgressBar(QWidget* parent = nullptr) : QProgressBar(parent), mGradientRatio("#ffffff"){ }

	void setValue(int value)
	{
		QColor color;
		if(colorAtValue(value, color)) setBackGroundColor(color);
	}

	void setBackGroundColor(const QColor& color)
	{
		char s[256];

		double ratioRed = (double)mGradientRatio.red() / 255.0;
		double ratioGreen = (double)mGradientRatio.green() / 255.0;
		double ratioBlue = (double)mGradientRatio.blue() / 255.0;

		sprintf
		(
			s,
			"QProgressBar::chunk{ background: QLinearGradient(x1: 0, y1: 0, x2: 1, y2: 0, stop: 0 #%02x%02x%02x, stop: %f #%02x%02x%02x); }",
			(int)(ratioRed * color.red()), (int)(ratioGreen * color.green()), (int)(ratioBlue * color.blue()), (double)value() / (double)maximum(),
			color.red(), color.green(), color.blue()
		);
		setStyleSheet(s);
	}

	void setGradientStyle1()
	{
		mKeyColors[0] = QColor("#ff0000");
		mKeyColors[50] = QColor("#ffff00");
		mKeyColors[100] = QColor("#00ff00");
		mGradientRatio = QColor(0.7 * 255.0, 0.3 * 255.0, 0.7 * 255.0);
	}

	void setGradientStyle2()
	{
		mKeyColors[0] = QColor("#e5405e");
		mKeyColors[45] = QColor("#ffdb3a");
		mKeyColors[100] = QColor("#3fffa2");
		mGradientRatio = QColor(0.7 * 255.0, 0.3 * 255.0, 0.7 * 255.0);
	}

	void setGradientStyle3()
	{
		mKeyColors[0] = QColor("#e5405e");
		mKeyColors[45] = QColor("#ffdb3a");
		mKeyColors[90] = QColor("#3fdfa2");
		mKeyColors[100] = QColor("#1fff35");
		mGradientRatio = QColor(0.7 * 255.0, 0.3 * 255.0, 0.7 * 255.0);
	}

	void setGradientStyle4()
	{
		mKeyColors[0] = QColor("#e5405e");
		mKeyColors[25] = QColor("#ffdb3a");
		mKeyColors[50] = QColor("#3fffa2");
		mKeyColors[73] = QColor("#1a9be0");
		mKeyColors[100] = QColor("#ba68ed");
		mGradientRatio = QColor(0.7 * 255.0, 0.3 * 255.0, 0.7 * 255.0);
	}
	void setGradientStyle5()
	{
		mKeyColors[0] = QColor("#438600");
		mKeyColors[100] = QColor("#8fd747");
		mGradientRatio = QColor(0.7 * 255.0, 0.3 * 255.0, 0.7 * 255.0);
	}

	void setGradientStyle6()
	{
		mKeyColors[0] = QColor("#b98cf5");
		mKeyColors[50] = QColor("#f0e6ff");
		mKeyColors[100] = QColor("#b98cf5");
	}

	void setGradientStyle7()
	{
		mKeyColors[0] = QColor(255, 197, 120);
		mKeyColors[100] = QColor(244, 128, 38);
	}
};

#endif
