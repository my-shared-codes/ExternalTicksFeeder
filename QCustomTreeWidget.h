#ifndef QCUSTOMTREEWIDGET_INCLUDED
#define QCUSTOMTREEWIDGET_INCLUDED

#include <QTreeWidget>
#include <QPoint>
#include <QMouseEvent>

class QCustomTreeWidget : public QTreeWidget
{
	Q_OBJECT


public:
	QPoint mMouseRelesePosition;

	explicit QCustomTreeWidget(QWidget *parent = nullptr) :QTreeWidget(parent) {}

	void mouseReleaseEvent(QMouseEvent* e)
	{
		mMouseRelesePosition = e->pos();
		QTreeWidget::mouseReleaseEvent(e);
	}

};

#endif
