//#include <QTextDocument>
//#include <QTimeZone>
#include <QJsonDocument>
#include "QDukascopyTickProvider.h"
//#include "ParserDom.h"
#include "QSymbol.h"

#include "ExternalTicksFeeder.h"
#include <cassert>

#ifdef WIN32
#define strcasecmp stricmp
#endif

void QDukascopyTickProvider::downloadSymbolList()
{
	if (!mNetworkaccessmanager)
	{
		mNetworkaccessmanager = new QNetworkAccessManager;
		mNetworkaccessmanager->setRedirectPolicy(QNetworkRequest::NoLessSafeRedirectPolicy);
		connect(mNetworkaccessmanager, &QNetworkAccessManager::finished, this, &QDukascopyTickProvider::on_Networkaccessmanager_finished);
		connect(mNetworkaccessmanager, &QNetworkAccessManager::sslErrors, this, &QDukascopyTickProvider::on_Networkaccessmanager_sslErrors);
	}

	//QNetworkRequest networkRequest(QUrl("https://tickstory.com/dukascopy-historical-data-available-date-ranges"));
	QNetworkRequest networkRequest(QUrl("https://freeserv.dukascopy.com/2.0/index.php?path=common%2Finstruments"));
	networkRequest.setRawHeader("Referer", "https://freeserv.dukascopy.com/");
	QNetworkReply* networkReply = mNetworkaccessmanager->get(networkRequest);

	if (!networkReply) return /*false*/;

	networkReply->setUserData(0, (QObjectUserData*)SYMBOL_LIST_DOWNLOAD);
	networkReply->ignoreSslErrors();

	connect(networkReply, &QNetworkReply::downloadProgress, this, &QDukascopyTickProvider::on_Networkaccessmanager_downloadProgress);
	connect(networkReply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(on_Networkaccessmanager_error(QNetworkReply::NetworkError)));
}

void QDukascopyTickProvider::on_Networkaccessmanager_downloadProgress(qint64 bytesReceived, qint64 bytesTotal)
{

}

void QDukascopyTickProvider::on_Networkaccessmanager_error(QNetworkReply::NetworkError code)
{
	//TODO: log / print error msg retry download with a timer
	assert(false);
}

void QDukascopyTickProvider::on_Networkaccessmanager_finished(QNetworkReply* reply)
{
	if (reply->error() != QNetworkReply::NoError)
	{
		on_Networkaccessmanager_error(reply->error());
		reply->deleteLater();
		return;
	}
	else if (reply->bytesAvailable() == 0)
	{
		on_Networkaccessmanager_error(reply->error());
		reply->deleteLater();
		return;
	}
	else if (reply->userData(0) == (QObjectUserData*)SYMBOL_LIST_DOWNLOAD)
	{
		QString jsonString = ;
		QJsonDocument json;
		json.fromJson(reply->readAll());

/*
		htmlcxx::HTML::ParserDom htmlParser;
		tree<htmlcxx::HTML::Node> dom = htmlParser.parseTree(reply->readAll().toStdString());
		reply->deleteLater();

		bool isInTable = false;
		bool isStrong = false;
		bool isTd = false;
		int tdIndex = 0;
		QSymbol symbol;
		QString path;

		for (const htmlcxx::HTML::Node& i : dom)
		{
			if(i.isComment()) continue;
			else if (i.isTag())
			{
				QString tagName = QString::fromStdString(i.tagName()).trimmed();

				if (tagName.isEmpty()) continue;

				else if (tagName.compare(tagName, "table", Qt::CaseInsensitive) == 0) isInTable = true;
				else if (tagName.compare(tagName, "/table", Qt::CaseInsensitive) == 0) isInTable = false;

				if (!isInTable) continue;

				isStrong = false;
				isTd = false;

				if (tagName.compare(tagName, "strong", Qt::CaseInsensitive) == 0) isStrong = true;
				else if (tagName.compare(tagName, "td", Qt::CaseInsensitive) == 0) isTd = true;
			}
			else
			{
				if (!isInTable) continue;

				QString text = QString::fromStdString(i.text()).trimmed();
				QTextDocument textDocument;
				textDocument.setHtml(text);
				text = textDocument.toPlainText();

				if (text.isEmpty()) continue;
				else if (isStrong)
				{
					path = text;
				}

				if (isTd && tdIndex == 0)
				{
					symbol.mName = text;
					symbol.mPath = path + "\\" + text;
					tdIndex++;
				}
				else if (isTd && tdIndex == 1)
				{
					symbol.mDescription = text;
					tdIndex++;
				}
				else if (isTd && tdIndex == 2)
				{
					if (parseFromDateTime(text, symbol)) mSymbols.push_back(symbol);
					tdIndex = 0;
				}
			}
		}

		mSymbols.shrink_to_fit();
		qStableSort(mSymbols.begin(), mSymbols.end(), &QSymbol::lessThen);

		mExternalTicksFeeder->fillTreeWidgets();
		return;*/
	}
	else if (reply->userData(0) == (QObjectUserData*)TICKS_DOWNLOAD)
	{
		//TODO
		reply->deleteLater();
		return;
	}
	//TODO
	assert(false);
	reply->deleteLater();
}

void QDukascopyTickProvider::on_Networkaccessmanager_sslErrors(QNetworkReply* reply, const QList<QSslError>& errors)
{
	//TODO: log / print error msg retry download with a timer
	assert(false);
}

bool QDukascopyTickProvider::parseFromDateTime(QString& str, QSymbol& result)
{
	str = str.replace(QRegExp("\\s{2,}"), " ");

	if (str.contains("(") || str.contains(")") || str.toLower().contains("discontinued")) return false;

	QRegExp regExp("[0-9]{4,}\\.[0-9]{1,2}\\.[0-9]{1,2}\\s*[0-9]{1,2}:[0-9]{1,2}");

	if (regExp.indexIn(str) != -1)
	{
		if (regExp.capturedTexts().isEmpty()) return false;

		int y = 0; int M = 0; int d = 0; int h = 0; int m = 0;
		if (sscanf(regExp.capturedTexts()[0].toLocal8Bit().constData(), "%04d.%02d.%02d %02d:%02d", &y, &M, &d, &h, &m) != 5)  return false;

		result.mFromDateTime = QDateTime(QDate(y, M, d), QTime(h, m), QTimeZone(0));
		result.mToDateTime = QDateTime::fromMSecsSinceEpoch(QDateTime::currentMSecsSinceEpoch()).toUTC();

		return true;
	}

	regExp.setPattern("[0-9]{4,}\\.[0-9]{1,2}\\.[0-9]{1,2}");

	if (regExp.indexIn(str) != -1)
	{
		if (regExp.capturedTexts().isEmpty()) return false;

		int y = 0; int M = 0; int d = 0;

		if (regExp.capturedTexts().size() >= 1)
		{
			if (sscanf(regExp.capturedTexts()[0].toLocal8Bit().constData(), "%04d.%02d.%02d", &y, &M, &d) != 3)  return false;
			result.mFromDateTime = QDateTime(QDate(y, M, d), QTime(0, 0), QTimeZone(0));
		}
		if (regExp.capturedTexts().size() >= 2)
		{
			if (sscanf(regExp.capturedTexts()[1].toLocal8Bit().constData(), "%04d.%02d.%02d", &y, &M, &d) != 3)  return false;
			result.mToDateTime = QDateTime(QDate(y, M, d), QTime(0, 0), QTimeZone(0));
		}

		return true;
	}

	return false;
}
