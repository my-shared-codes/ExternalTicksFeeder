#ifndef QMT5TERMINALTICKPROVIDER_INCLUDED
#define QMT5TERMINALTICKPROVIDER_INCLUDED

#include "ITickProvider.h"

struct QMT5TerminalTickProvider : public ITickProvider
{
	explicit QMT5TerminalTickProvider(const QString& name, QObject* parent = nullptr) { mName = name; setParent(parent); }
	virtual ~QMT5TerminalTickProvider() {}

	virtual void downloadSymbolList() override;

	virtual void on_Networkaccessmanager_downloadProgress(qint64 bytesReceived, qint64 bytesTotal) override;
	virtual void on_Networkaccessmanager_error(QNetworkReply::NetworkError code) override;
	virtual void on_Networkaccessmanager_finished(QNetworkReply* reply) override;
	virtual void on_Networkaccessmanager_sslErrors(QNetworkReply* reply, const QList<QSslError>& errors) override;
};

#endif
