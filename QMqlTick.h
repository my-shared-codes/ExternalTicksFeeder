#ifndef QMQLTICK_INCLUDED
#define QMQLTICK_INCLUDED

#include <QtGlobal>

#pragma pack(push, 1)
struct QMqlTick
{
	quint64	time;
	double bid;
	double ask;
	double last;
	quint64 volume;
	qint64 time_msc;
	quint32	flags;
	double volume_real;
};
#pragma pack(pop)

#endif
