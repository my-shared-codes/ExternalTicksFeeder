#include "QSymbol.h"

bool QSymbol::lessThen(const QSymbol& l, const QSymbol& r)
{
	return l.mPath.compare(r.mPath, Qt::CaseInsensitive) < 0;
}
