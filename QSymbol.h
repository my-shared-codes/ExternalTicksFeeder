#ifndef QSYMBOL_INCLUDED
#define QSYMBOL_INCLUDED

#include <QtGlobal>
#include <QString>
#include <QDateTime>

struct QSymbol
{
	QString mName;
	QString mDescription;
	QString mMT5ClonedSymbolName;
	int mMT5ClonedSymbolIndex;
	double mMT5ClonedSymbolSimilarity;
	QString mPath;
	uint8_t mDigits;
	QDateTime mFromDateTime;
	QDateTime mToDateTime;
	QSymbol() :mMT5ClonedSymbolIndex(-1), mMT5ClonedSymbolSimilarity(0.0) {}
	static bool QSymbol::lessThen(const QSymbol& l, const QSymbol& r);
	bool operator==(const QSymbol& r) const { return mPath.compare(r.mPath, Qt::CaseInsensitive) == 0; }
};

#endif
