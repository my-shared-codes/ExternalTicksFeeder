#include "QTickProviderFactory.h"
#include "QDukascopyTickProvider.h"
#include "QMT5TerminalTickProvider.h"
#include "QRannForexTickProvider.h"
#include "QAlpariTickProvider.h"

ITickProvider* QTickProviderFactory::createTickProvider(QString name, QObject* parent)
{
	if (name == "MT5 Terminal") return new QMT5TerminalTickProvider(name, parent);

	if (name == "Dukascopy") return new QDukascopyTickProvider(name, parent);

	if (name == "RannForex") return new QRannForexTickProvider(name, parent);

	if (name == "Alpari") return new QAlpariTickProvider(name, parent);

	return nullptr;
}
