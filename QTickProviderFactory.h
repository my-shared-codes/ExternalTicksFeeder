#ifndef QTICKPROVIDERFACTORY_INCLUDED
#define QTICKPROVIDERFACTORY_INCLUDED

#include "ITickProvider.h"

struct QTickProviderFactory
{
	static ITickProvider* createTickProvider(QString name, QObject* parent);
};

#endif
