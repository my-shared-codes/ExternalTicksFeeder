#ifndef QTREESET_INCLUDED
#define QTREESET_INCLUDED

#include <QVector>

template<typename T> struct QTreeSetNode
{
	T mData;
	QTreeSetNode<T>* mParent;
	QVector<QTreeSetNode<T>*> mChildren;

	QTreeSetNode(const T& data = T(), QTreeSetNode<T>* parent = nullptr) : mData(data), mParent(parent) {}
	~QTreeSetNode() { clear(); }

	QTreeSetNode<T>* addChild(const T& data)
	{
		for (QTreeSetNode<T>* i : mChildren) if (i->mData == data) return i;
		QTreeSetNode<T>* newNode = new QTreeSetNode<T>(data, this);
		mChildren.push_back(newNode);
		return newNode;
	}

	void clear() { for (QTreeSetNode<T>* i : mChildren) delete i; }

	void shrink_to_fit()
	{
		mChildren.shrink_to_fit();
		for (QTreeSetNode<T>* i : mChildren) i->shrink_to_fit();
	}

	void print()
	{
		static int level = 0;
		qDebug() << QString(level * 8, ' ').toLocal8Bit().constData() << mData;
		level++;
		for (QTreeSetNode<T>* i : mChildren) i->print();
		level--;
	}
};

#endif QTREESET_INCLUDED
