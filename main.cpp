#include "ExternalTicksFeeder.h"
#include <QApplication>
#include <QDateTime>
#include <QSharedMemory>
#include <QMessageBox>
#include <QDir>

#ifdef WIN32
#include <windows.h>
#include <QThread>
#endif

/*
	for (QTimeZone::OffsetData& i : QTimeZone("America/New_York").transitions(QDateTime::fromMSecsSinceEpoch(0), QDateTime::currentDateTime()))
		qDebug() << i.atUtc.toTimeZone(QTimeZone("America/New_York")) << "        " << i.daylightTimeOffset;
*/

static void bringApplicationToFront()
{
#ifdef WIN32
	HWND hwnd = nullptr;
	for (int i = 0; i < 100; ++i)
	{
		hwnd = FindWindowW(nullptr, L"ExternalTicksFeeder");
		if (hwnd)
		{
			SetForegroundWindow(hwnd);
			BringWindowToTop(hwnd);
			SetFocus(hwnd);
			break;
		}
		else
		{
			QThread::msleep(100);
		}
	}
#else
	QMessageBox::information(nullptr, "ExternalTicksFeeder", "Another instance of ExternalTicksFeeder is already running.", QMessageBox::Ok);
#endif
}

int main(int argc, char** argv)
{
	srand((unsigned int)QDateTime::currentMSecsSinceEpoch());
	QApplication a(argc, argv);

	QSharedMemory singleInstanceGuard("ExternalTicksFeeder_41e1a3a3-e48f-44f2-b613-ebfb80f4f6ee");
	if (!singleInstanceGuard.create(sizeof(void*)))
	{
		if (singleInstanceGuard.error() == QSharedMemory::AlreadyExists)
		{
			bringApplicationToFront();
		}
		else
		{
			QString msgBoxText = "Unable to create QSharedMemory object\nError: " + QSharedMemory().errorString() + "\nErrorcode: " + singleInstanceGuard.error();
			QMessageBox::critical(nullptr, "ExternalTicksFeeder", msgBoxText, QMessageBox::Ok);
		}
		return 0;
	}

	a.setWindowIcon(QIcon("icons/ExternalTicksFeeder.png"));

	ExternalTicksFeeder w;

	QFile qssFile("skins/qdarkstyle/style.qss");
	if(qssFile.open(QFile::ReadOnly)) w.setStyleSheet(qssFile.readAll());
	qssFile.close();

	w.showMaximized();

	return a.exec();
}
